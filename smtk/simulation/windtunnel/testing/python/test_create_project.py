# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.operation
import smtk.project
import smtk.resource
import smtk.testing

PROJECT_NAME = 'foam.windtunnel'
OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)


class TestProjectCreate(smtk.testing.TestCase):
    def setUp(self):
        smtk.workflows_folder = os.path.join(smtk.testing.SOURCE_DIR, 'simulation-workflows')

        # Initialize resource & operation managers
        self.res_manager = smtk.resource.Manager.create()
        self.op_manager = smtk.operation.Manager.create()

        smtk.attribute.Registrar.registerTo(self.res_manager)
        smtk.attribute.Registrar.registerTo(self.op_manager)

        smtk.operation.Registrar.registerTo(self.op_manager)
        self.op_manager.registerResourceManager(self.res_manager)

        self.proj_manager = smtk.project.Manager.create(self.res_manager, self.op_manager)
        smtk.project.Registrar.registerTo(self.proj_manager)

        self.proj_manager.registerProject(PROJECT_NAME, set(), set(), '1.0.0')

        self.managers = smtk.common.Managers.create()
        self.managers.insert_or_assign(self.res_manager)
        self.managers.insert_or_assign(self.op_manager)
        self.managers.insert_or_assign(self.proj_manager)
        self.op_manager.setManagers(self.managers)

    def tearDown(self):
        self.res_manager = None
        self.op_manager = None
        self.proj_manager = None

    def test_create(self):
        """"""
        # Import the test operation
        import_op = self.op_manager.createOperation('smtk::operation::ImportPythonOperation')
        self.assertIsNotNone(import_op)

        path = os.path.join(
            smtk.testing.SOURCE_DIR, 'smtk/simulation/windtunnel/operations/create_project.py')
        self.assertTrue(os.path.exists(path), 'operation file not found: {}'.format(path))

        import_op.parameters().findFile("filename").setValue(path)
        import_res = import_op.operate()
        import_outcome = import_res.findInt('outcome').value()
        self.assertEqual(import_outcome, OP_SUCCEEDED,
                         'import python op outcome {}'.format(import_outcome))

        # Instantiate and run the create operation
        op_name = import_res.findString('unique_name').value()
        create_op = self.op_manager.createOperation(op_name)
        self.assertIsNotNone(create_op, 'Create operation is null')

        project_dir = os.path.join(smtk.testing.TEMP_DIR, 'python', 'test_create')
        if os.path.exists(project_dir):
            print('Removing directory', project_dir)
            shutil.rmtree(project_dir)
        # create_op.parameters().findVoid('overwrite').setIsEnabled(True)
        create_op.parameters().findDirectory('directory').setValue(project_dir)
        # create_op.parameters().findVoid('taskflow').setIsEnabled(True)
        create_result = create_op.operate()
        outcome = create_result.findInt('outcome').value()
        if outcome != OP_SUCCEEDED:
            print(create_op.log().convertToString())
            self.assertTrue(False, 'operation outcome {}'.format(outcome))

        # Check that project was created
        project = create_result.findResource('resource').value()
        self.assertIsNotNone(project, 'project is null')

        # Verify project file was created
        project_file = os.path.join(project_dir, 'test_create.project.smtk')
        self.assertTrue(os.path.exists(project_file))


if __name__ == '__main__':
    smtk.testing.process_arguments()
    smtk.testing.main()
