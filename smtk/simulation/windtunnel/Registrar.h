//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_windtunnel_Registrar_h
#define smtk_simulation_windtunnel_Registrar_h

#include "smtk/simulation/windtunnel/Exports.h"

#include "smtk/project/Manager.h"

#include <string>
#include <vector>

namespace smtk
{
namespace simulation
{
namespace windtunnel
{
class SMTKWINDTUNNEL_EXPORT Registrar
{
public:
  static void registerTo(const smtk::project::Manager::Ptr&);
  static void unregisterFrom(const smtk::project::Manager::Ptr&);

private:
  // Store python op names when loaded (for unloading later)
  static std::vector<std::string> s_pythonOperationNames;
};

} // namespace windtunnel
} // namespace simulation
} // namespace smtk

#endif
