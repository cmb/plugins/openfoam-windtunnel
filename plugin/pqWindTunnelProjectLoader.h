//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pqWindTunnelProjectLoader_h
#define pqWindTunnelProjectLoader_h

#include "smtk/PublicPointerDefs.h"

#include <QObject>

class pqServer;
class pqServerResource;

/** \brief Handles opening projects from main menu and recent projects list
  */
class pqWindTunnelProjectLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  static pqWindTunnelProjectLoader* instance(QObject* parent = nullptr);
  ~pqWindTunnelProjectLoader() override;

  // Indicates if paraview resource can be loaded
  bool canLoad(const pqServerResource& resource) const;

  // Open project from path on server
  bool load(pqServer* server, const QString& path);

  // Open project from recent projects list
  bool load(const pqServerResource& resource);

Q_SIGNALS:
  void projectOpened(smtk::project::ProjectPtr);

protected:
  pqWindTunnelProjectLoader(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWindTunnelProjectLoader);
};

#endif // pqWindTunnelProjectLoader_h
