//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelSaveProjectBehavior_h
#define plugin_pqWindTunnelSaveProjectBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing an ace3p project to disk.
class pqWindTunnelSaveProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWindTunnelSaveProjectReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWindTunnelSaveProjectReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWindTunnelSaveProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWindTunnelSaveProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWindTunnelSaveProjectBehavior() override;

  bool saveProject();

protected:
  pqWindTunnelSaveProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWindTunnelSaveProjectBehavior);
};

#endif
