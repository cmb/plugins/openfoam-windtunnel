//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelAutoStart_h
#define plugin_pqWindTunnelAutoStart_h

#include <QObject>

class pqServer;
class pqSMTKWrapper;
class pqWindTunnelContainerEngineSetup;

class pqWindTunnelAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqWindTunnelAutoStart(QObject* parent = nullptr);
  ~pqWindTunnelAutoStart() = default;

  void startup();
  void shutdown();

protected Q_SLOTS:
  void resourceManagerAdded(pqSMTKWrapper* mgr, pqServer* server);
  void resourceManagerRemoved(pqSMTKWrapper* mgr, pqServer* server);

  /** \brief Wait for other parts of the app to start
   *
   * Before initializing, this plugin needs:
   *   - the main widget to be instantiated
   *   - the plugin location to be resolved
   *   - smtk settings are instantiated
   *   - the resource manager to be added
   */
  void waitForApplication();

  /** \brief Runs steps to find and configure container engine. */
  void setupContainerEngine();

  /** \brief Registers operations */
  void registerOperations();

protected:
  void initializeSettings(pqServer* server);

  pqWindTunnelContainerEngineSetup* m_containerSetup = nullptr;

private:
  Q_DISABLE_COPY(pqWindTunnelAutoStart);
};

#endif
