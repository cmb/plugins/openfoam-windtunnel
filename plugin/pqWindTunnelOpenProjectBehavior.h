//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelOpenProjectBehavior_h
#define plugin_pqWindTunnelOpenProjectBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for opening a windtunnel project
class pqWindTunnelOpenProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWindTunnelOpenProjectReaction(QAction* parent);

  void openProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->openProject(); }

private:
  Q_DISABLE_COPY(pqWindTunnelOpenProjectReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWindTunnelOpenProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWindTunnelOpenProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWindTunnelOpenProjectBehavior() override;

protected:
  pqWindTunnelOpenProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWindTunnelOpenProjectBehavior);
};

#endif
