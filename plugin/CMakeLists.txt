set(ui_files
  pqWindTunnelControlsView.ui
)

  set(client_headers
  pqWindTunnelAutoStart.h
  pqWindTunnelContainerEngineSetup.h
  pqWindTunnelCloseProjectBehavior.h
  pqWindTunnelControlsView.h
  pqWindTunnelMenu.h
  pqWindTunnelNewProjectBehavior.h
  pqWindTunnelOpenProjectBehavior.h
  pqWindTunnelPluginLocation.h
  pqWindTunnelProjectLoader.h
  pqWindTunnelRecentProjectsMenu.h
  pqWindTunnelSaveProjectBehavior.h
)

set(client_sources
  pqWindTunnelAutoStart.cxx
  pqWindTunnelContainerEngineSetup.cxx
  pqWindTunnelCloseProjectBehavior.cxx
  pqWindTunnelControlsView.cxx
  pqWindTunnelMenu.cxx
  pqWindTunnelNewProjectBehavior.cxx
  pqWindTunnelOpenProjectBehavior.cxx
  pqWindTunnelPluginLocation.cxx
  pqWindTunnelProjectLoader.cxx
  pqWindTunnelRecentProjectsMenu.cxx
  pqWindTunnelSaveProjectBehavior.cxx
)

set(CMAKE_AUTOUIC 1)
set(CMAKE_AUTOMOC 1)

# Auto start class
paraview_plugin_add_auto_start(
  CLASS_NAME pqWindTunnelAutoStart
  INTERFACES auto_start_interfaces
  SOURCES auto_start_sources
)

# Plugin location
paraview_plugin_add_location(
    CLASS_NAME pqWindTunnelPluginLocation
    INTERFACES location_interfaces
    SOURCES location_sources
  )

# WindTunnel menu
paraview_plugin_add_action_group(
  CLASS_NAME pqWindTunnelMenu
  GROUP_NAME "MenuBar/WindTunnel"
  INTERFACES menu_interfaces
  SOURCES menu_sources
)

# WindTunnel plugin
paraview_add_plugin(smtkWindTunnelPlugin
  VERSION "1.0"
  UI_INTERFACES
    ${auto_start_interfaces}
    ${location_interfaces}
    ${menu_interfaces}
  SOURCES
    ${client_headers}
    ${client_sources}
    ${auto_start_sources}
    ${location_sources}
    ${menu_sources}
  UI_RESOURCES
    ${ui_files}
    icons/icons.qrc
  SERVER_MANAGER_XML
    WindTunnelSettings.xml
  XML_DOCUMENTATION OFF
)

if (DEVELOPER_MODE)
  target_compile_definitions(smtkWindTunnelPlugin
    PRIVATE
      "WORKFLOWS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/simulation-workflows\""
      "OPERATIONS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/smtk/simulation/windtunnel/operations\""
)
endif()

target_link_libraries(smtkWindTunnelPlugin
  LINK_PUBLIC
    smtkWindTunnelQtExt
    smtkWindTunnel
    smtkCore
    smtkPQComponentsExt
    smtkPVServerExt
    ParaView::pqApplicationComponents
    ParaView::pqComponents
    Qt5::Core
)
target_compile_definitions(smtkWindTunnelPlugin PRIVATE QT_NO_KEYWORDS)

target_include_directories(smtkWindTunnelPlugin PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
)
target_compile_definitions(
  smtkWindTunnelPlugin PUBLIC RECENTLY_USED_PROJECTS_TAG="SMTK_PROJECT"
)
