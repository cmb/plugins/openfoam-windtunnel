//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "plugin/pqWindTunnelPluginLocation.h"

#include "smtk/simulation/windtunnel/Metadata.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>

pqWindTunnelPluginLocation::pqWindTunnelPluginLocation(QObject* parent)
  : Superclass(parent)
{
}

pqWindTunnelPluginLocation::~pqWindTunnelPluginLocation() {}

void pqWindTunnelPluginLocation::StoreLocation(const char* fileLocation)
{
  if (fileLocation == nullptr)
  {
    qDebug()
      << "Internal Error: pqWindTunnelPluginLocation::StoreLocation() called with null pointer";
    return;
  }

  QFileInfo fileInfo(fileLocation);
  QDir dirLocation(fileInfo.absoluteDir());

#ifndef NDEBUG
  qDebug() << "Plugin location: " << fileLocation;
  qDebug() << "Plugin directory: " << dirLocation.path();
#endif

#ifndef DEVELOPER_MODE
  this->locateWorkflows(dirLocation);
#endif
}

void pqWindTunnelPluginLocation::locateWorkflows(const QDir& pluginDirectory) const
{
  // Save starting directory for diagnostics
  QDir startingDir(pluginDirectory);

  // Look for path to simulation workflows
  // * Depends on superbuild-packaging organization
  // * Which is platform-dependent
  QString relativePath;
#if defined(_WIN32)
  relativePath = "../../../share/cmb";
#elif defined(__APPLE__)
  relativePath = "../Resources";
#elif defined(__linux__)
#ifdef NDEBUG
  relativePath = "../../../share/cmb";
#else
  // Alternate path for development
  relativePath = "../../../install/share";
#endif
#endif

  if (relativePath.isEmpty())
  {
#ifdef NDEBUG
    qCritical() << "Missing installed path to workflow files";
#endif
    return;
  }

  // Locate path to workflows
  QDir dirLocation(pluginDirectory);
  bool exists = dirLocation.cd(relativePath);
  if (!exists)
  {
    QString path = startingDir.path() + "/" + relativePath;
    qCritical() << "Plugin workflows directory not found:" << path
                << "\n==> Do you need to set DEVELOPER_MODE (cmake variable)?";
    return;
  }

  // Locate workflows directory
  {
    QDir dir(dirLocation);
    QString relativePath("workflows/WindTunnel/workflows");
    bool exists = dir.cd(relativePath);
    if (exists)
    {
#ifndef NDEBUG
      qDebug() << "Setting workflows directory to" << dir.absolutePath();
#endif
      smtk::simulation::windtunnel::Metadata::WORKFLOWS_DIRECTORY =
        dir.absolutePath().toStdString();
    }
    else
    {
      qCritical() << "No workflows directory found for starting directory"
                  << dirLocation.absolutePath() << "and relative path" << relativePath;
    }
  }

  // Locate operations directory
  {
    QDir dir(dirLocation);
    QString relativePath("workflows/WindTunnel/operations");
    bool exists = dir.cd(relativePath);
    if (exists)
    {
#ifndef NDEBUG
      qDebug() << "Setting operations directory to" << dir.absolutePath();
#endif
      smtk::simulation::windtunnel::Metadata::OPERATIONS_DIRECTORY =
        dir.absolutePath().toStdString();
    }
    else
    {
      qCritical() << "No operations directory found for starting directory"
                  << dirLocation.absolutePath() << "and relative path" << relativePath;
    }
  }
}
