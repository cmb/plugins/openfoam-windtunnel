//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelNewProjectBehavior_h
#define plugin_pqWindTunnelNewProjectBehavior_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include "pqReaction.h"

#include <QObject>

class pqWindTunnelNewProjectBehaviorInternals;

/// \brief A reaction for creating a new ACE3P project.
class pqWindTunnelNewProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqWindTunnelNewProjectReaction(QAction* parent);
  ~pqWindTunnelNewProjectReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWindTunnelNewProjectReaction)
};

/// \brief Creates new ACE3P project.
class pqWindTunnelNewProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWindTunnelNewProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWindTunnelNewProjectBehavior() override;

  void newProject();

Q_SIGNALS:
  void projectCreated(smtk::project::ProjectPtr);

protected Q_SLOTS:
  void onOperationExecuted(const smtk::operation::Operation::Result& result);

protected:
  pqWindTunnelNewProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWindTunnelNewProjectBehavior);
};

#endif
