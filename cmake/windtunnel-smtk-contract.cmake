# Contract file for testing from SMTK.

cmake_minimum_required(VERSION 3.12)
project(openfoam-windtunnel)

include(ExternalProject)

ExternalProject_Add(openfoam-windtunnel
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/openfoam-windtunnel.git"
  GIT_TAG "origin/master"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_BEFORE_INSTALL True
)
