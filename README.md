Introduction
============
OpenFOAM Wind Tunnel is a CMB/modelbuilder plugin for modeling wind tunnel
problems using [OpenFOAM](https://www.openfoam.com) for meshing and
computational fluid dynamics (CFD).


**IMPORTANT** When developing, be sure to enable the **DEVELOPER_MODE** flag,
in order for modelbuilder to find the simulation workflow files at runtime.

CMB is an open-source, multi-platform simulation workflow framework based on
[ParaView][], [Visualization Toolkit (VTK)][VTK] and [Qt][].


[CMB]: https://www.ComputationalModelBuilder.org
[ParaView]: https://www.paraview.org
[QT]: https://www.qt.io
[VTK]: http://www.vtk.org
[Kitware]: https://www.kitware.com

License
========

CMB and this plugin are distributed under the OSI-approved BSD 3-clause
License. See [License.txt][] for details.

[License.txt]: LICENSE.txt
