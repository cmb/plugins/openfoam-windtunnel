<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">

  <Categories>
    <Cat>background</Cat>
    <Cat>target</Cat>
    <Cat>icoFoam</Cat>
    <Cat>simpleFoam</Cat>
  </Categories>

  <Analyses Exclusive="true">
    <Analysis Type="icoFoam">
      <Cat>icoFoam</Cat>
      <Cat>target</Cat>
    </Analysis>
    <Analysis Type="simpleFoam">
      <Cat>simpleFoam</Cat>
      <Cat>target</Cat>
    </Analysis>
    <Analysis Type="background">
      <Cat>background</Cat>
    </Analysis>
    <Analysis Type="target">
      <Cat>target</Cat>
    </Analysis>
  </Analyses>

  <!--********** Include files **********-->
  <Includes>
    <File>internal/templates/controlDict.sbt</File>
    <File>internal/templates/background.sbt</File>
    <File>internal/templates/target.sbt</File>
    <File>internal/templates/snappy.sbt</File>
    <File>internal/templates/solver.sbt</File>
    <File>internal/templates/refineMesh.sbt</File>
<!--     <File>internal/templates/refinementRegions.sbt</File> -->
  </Includes>

  <ItemBlocks>
    <!-- resource-categories indicate which resources are in the project -->
    <!-- They are copied to each Analysis group item (icoFoam, simpleFoam) -->
    <Block Name="resource-categories">
      <ItemDefinitions>
        <Void Name="background" Optional="true" IsEnabledByDefault="true" />
        <Void Name="target" Optional="true" IsEnabledByDefault="false" />
      </ItemDefinitions>
    </Block>
  </ItemBlocks>


  <Definitions>
    <!-- Explicity specify analysis attribute definition so that we can insert
         background and target analyses in both icoFoam and simpleFoam.

         Note that group items are marked AdvanceLevel 1 so that they do not
         appear in the standard UI. The contents of these groups are intended
         to be modified internally by the application software.

         Future: add support for pisoFoam (transient solver)
    -->
    <AttDef Type="Analysis">
      <ItemDefinitions>
        <String Name="Analysis" Label="Application" NumberOfRequiredValues="1">
          <ChildrenDefinitions>
            <Group Name="icoFoam" NumberOfRequiredGroups="1" AdvanceLevel="1">
              <ItemDefinitions>
                <Block Name="resource-categories" />
              </ItemDefinitions>
            </Group>
            <!-- <Group Name="simpleFoam" NumberOfRequiredGroups="1" AdvanceLevel="1">
              <ItemDefinitions>
                <Block Name="resource-categories" />
              </ItemDefinitions>
            </Group> -->
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="icoFoam">icoFoam</Value>
              <Items>
                <Item>icoFoam</Item>
              </Items>
            </Structure>
            <!-- <Structure>
              <Value Enum="simpleFoam">simpleFoam</Value>
              <Items>
                <Item>simpleFoam</Item>
              </Items>
            </Structure> -->
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

  </Definitions>


  <!--********** Views **********-->
  <Views>
    <View Type="Group" Title="Wind Tunnel" TopLevel="true" TabPosition="North"
     AttributePanelTitle="OpenFOAM" FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <!-- <View Title="Control" /> -->
        <View Title="ModelGroup" />
        <View Title="BackgroundGroup" />
        <View Title="RefineMeshGroup" />
        <View Title="SnappyHexMeshGroup" />
        <View Title="SolverGroup" />
      </Views>
    </View>

    <View Type="Analysis" Title="Solver" Label="Solver"
      AnalysisAttributeName="Analysis" AnalysisAttributeType="Analysis" AnalysisAttributeLabel="Module">
    </View>

    <View Type="Group" Title="Control" Style="Tiled">
      <Views>
        <View Title="Solver" />
        <View Title="ControlDict" />
      </Views>
    </View>

    <View Type="Instanced" Title="ControlDict" Label="controlDict">
      <InstancedAttributes>
        <Att Type="controlDict" Name="controlDict" />
      </InstancedAttributes>
    </View>


    <!-- Block Mesh tab -->
    <View Title="BackgroundGeometry" Type="Instanced" Label="Geometry">
      <InstancedAttributes>
        <!-- <Att Type="Scale" Name="Scale" /> -->
        <Att Type="BoxWidget" Name="BackgroundGeometry">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Title="BackgroundMeshSize" Type="Instanced" Label="Mesh Size">
      <InstancedAttributes>
        <Att Type="BlockMeshSize" Name="BlockMeshSize" />
      </InstancedAttributes>
    </View>

    <View Title="BackgroundBoundaryConditions" Type="Instanced" Label="Boundary Conditions">
      <InstancedAttributes>
        <Att Type="BlockMeshBoundaryConditions" Name="BlockMeshBoundaryConditions" />
      </InstancedAttributes>
    </View>

    <View Name="BackgroundControls" Type="WindTunnelControls" Label="blockMesh">
      <Operation>blockmesh.BlockMesh</Operation>
      <FoamApplication>blockMesh</FoamApplication>
      <Geometry Type="FoamFile" Representation="Surface With Edges">blockMesh.foam</Geometry>
    </View>

    <View Name="BackgroundGroup" Type="Group" Label="Block Mesh" Style="Tiled">
      <Views>
        <View Title="BackgroundGeometry" />
        <View Title="BackgroundMeshSize" />
        <View Title="BackgroundBoundaryConditions" />
        <View Title="BackgroundControls" />
      </Views>
    </View>


    <!-- Model tab -->
    <View Name="ModelControls" Type="WindTunnelControls" Label="Import Model">
      <Operation UseDialog="true">import_model.ImportModel</Operation>
      <Geometry Type="ResourceRole">model</Geometry>
    </View>

    <View Type="Instanced" Title="ModelInstance" Label="Surface Features">
      <InstancedAttributes>
        <Att Type="Target" Name="Target">
          <ItemViews>
            <View Path="/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Name="SurfaceFeatureControls" Type="WindTunnelControls" Label="surfaceFeatureExtract">
      <Operation>surfaceextract.SurfaceFeatureExtract</Operation>
      <FoamApplication CaseDirectory="snappyHexMesh">surfaceFeatureExtract</FoamApplication>
    </View>

    <View Name="ModelGroup" Type="Group" Label="Model" Style="Tiled">
      <Views>
        <View Title="ModelControls" />
        <View Title="ModelInstance" />
        <View Title="SurfaceFeatureControls" />
      </Views>
    </View>


    <!-- Refine Mesh tab -->
    <View Type="Instanced" Title="RefineMeshDirection" Label="Refinement Directions">
      <InstancedAttributes>
        <Att Type="RefineMeshDirection" Name="RefineMeshDirection">
          <ItemViews>
            <View Item="Enable" Layout="Horizontal" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="RefineMeshRegions" Label="Refinement Regions">
      <AttributeTypes>
        <Att Type="RefineMeshRegion" Name="Refinement Region">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
      </AttributeTypes>
    </View>

    <View Name="RefineMeshControls" Type="WindTunnelControls" Label="refineMesh">
      <Operation>refinemesh.RefineMesh</Operation>
      <FoamApplication Custom="true" CaseDirectory="blockMesh">refineMesh</FoamApplication>
      <Geometry Type="FoamFile" Representation="Surface With Edges">blockMesh.foam</Geometry>
    </View>

    <View Name="RefineMeshGroup" Type="Group" Label="Refine Mesh" Style="Tiled">
      <Views>
        <View Title="RefineMeshDirection" />
        <View Title="RefineMeshRegions" />
        <View Title="RefineMeshControls" />
      </Views>
    </View>


    <!-- Snappy Hex Mesh tab -->
    <View Type="Group" Title="Snappy" Label="Snappy Hex Mesh" TabPosition="North">
      <Views>
        <View Title="Castellation" />
        <View Title="Snapping" />
        <!-- <View Title="Refinement Regions" /> -->
      </Views>
    </View>

    <View Type="Instanced" Title="Castellation">
      <InstancedAttributes>
        <Att Type="Castellation" Name="Castellation">
          <ItemViews>
            <View Path="/Castellation/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Snapping" >
      <InstancedAttributes>
        <Att Type="SnapControls" Name="SnapControls" />
      </InstancedAttributes>
    </View>

    <!-- <Att Type="MeshQualityControls" Name="MeshQualityControls" /> -->

    <View Name="SnappyHexMeshControls" Type="WindTunnelControls" Label="snappyHexMesh">
      <Operation>snappyhexmesh.SnappyHexMesh</Operation>
      <FoamApplication>snappyHexMesh</FoamApplication>
      <FoamArguments><Argument>-overwrite</Argument></FoamArguments>
      <Geometry Type="FoamFile" Representation="Surface With Edges">snappy.foam</Geometry>
    </View>

    <View Name="SnappyHexMeshGroup" Type="Group" Label="Snappy Hex Mesh" Style="Tiled">
      <Views>
        <View Title="Snappy" />
        <View Title="SnappyHexMeshControls" />
      </Views>
    </View>


    <!-- Solver Group -->
    <View Type="Instanced" Title="Physics">
      <InstancedAttributes>
        <Att Type="PhysicalProperties" Name="PhysicalProperties" />
        <Att Type="VelocityBoundaryCondition" Name="VelocityBoundaryCondition" />
        <Att Type="PressureBoundaryCondition" Name="PressureBoundaryCondition" />
        <Att Type="TurbulenceModel" Name="TurbulenceModel" />
      </InstancedAttributes>
    </View>

    <View Name="IcoFoamControls" Type="WindTunnelControls" Label="icoFoam">
      <Category>icoFoam</Category>
      <Operation>icofoam.IcoFoam</Operation>
      <FoamApplication>icoFoam</FoamApplication>
      <Geometry Type="FoamFile">ico.foam</Geometry>
    </View>

    <View Name="SimpleFoamControls" Type="WindTunnelControls" Label="simpleFoam">
      <Category>simpleFoam</Category>
      <Operation>simplefoam.SimpleFoam</Operation>
      <FoamApplication>simpleFoam</FoamApplication>
      <Geometry Type="FoamFile">simple.foam</Geometry>
    </View>

    <View Name="SolverGroup" Type="Group" Label="Solver" Style="Tiled">
      <Views>
        <View Title="Physics" />
        <View Title="Solver" />
        <View Title="ControlDict" />
        <View Title="IcoFoamControls" />
        <View Title="SimpleFoamControls" />
      </Views>
    </View>

  </Views>

</SMTK_AttributeResource>
