#!/usr/bin/env bash

# Stops all docker containers
docker stop $(docker ps -q)
