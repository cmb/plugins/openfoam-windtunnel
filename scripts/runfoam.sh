#!/usr/bin/env bash

# Run this from the "foam" directory

# Exit when any command fails
set -e

echo Run blockMesh
cd ./mesh
openfoam-docker / blockMesh | tee blockMesh.log
touch mesh.foam

echo Run surfaceFeatureExtract
openfoam-docker / surfaceFeatureExtract | tee surfaceFeature.log

echo Run snappyHexMesh
openfoam-docker / snappyHexMesh -overwrite | tee snappy.log
touch mesh.foam

# Switch to icoFoam case directory
cd ../icoFoam

echo Copy blockMesh results
rm -rf constant/polyMesh
cp -r ../mesh/constant/polyMesh constant

echo Run icoFoam solver
openfoam-docker / icoFoam | tee icoFoam.log

echo Manually edit taskflow.json to show all tasks completed
