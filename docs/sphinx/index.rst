#################
CMB Wind Tunnel
#################

.. image:: ./images/wingsection-stream.png
    :align: center

|

To help new users become familiar with `Kitware <https://kitware.com>`_'s  `Computational Model Builder (CMB) <https://computationalmodelbuilder.org/>`_ platform, we put together this short tutorial using a customized version of the :program:`CMB modelbuilder` application. This tutorial will construct and carry out basic computational fluid dynamics (CFD) simulations with the popular `OpenFOAM <https://www.openfoam.com/>`_ simulation code. Instructions are provided for downloading the :program:`CMB modelbuilder` software and walking through example problems simulating wind tunnel physics. We have purposely kept the examples simple in order to focus on the features and benefits that :program:`CMB` provides for these kinds of workflows as well as not requiring the reader to have in depth knowledge of :program:`OpenFOAM`. We have also taken some shortcuts to save compute cycles.

.. note::
  More information about :program:`CMB` is available at http://computationalmodelbuilder.org

After completing these examples, you are welcome to experiment with various options in the user interface or try your own model files (``.obj``, ``.stl``, or ``.stlb``). We also added a :ref:`How It All Works` section to briefly describe how we are able to deploy CMB across many diverse simulation codes without writing new software from the ground up each time. The :program:`CMB` platform is not specifically designed for :program:`OpenFOAM` or focused on CFD problems alone. Because of its designed-in flexibility, :program:`CMB` is ideal and cost efficient for a wide range of simulation codes and workflows. So if you are interested in using :program:`CMB` and :program:`modelbuilder` for your simulation workflows, please get in touch with us at https://www.kitware.com/contact/general/.


.. toctree::
   :maxdepth: 1

   background.rst
   install.rst
   wing-section/index.rst
   ahmed-body/index.rst
   how-it-works/index.rst
