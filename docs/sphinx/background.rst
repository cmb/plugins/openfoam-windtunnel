Background
===========

Computational Model Builder (CMB)
--------------------------------

.. image:: ./images/CMB-Functional-Overview_LARGE1.png
    :align: right
    :width: 350px


:program:`CMB` is a software framework that supports the full numerical simulation life cycle --- from preprocessing to execution to postprocessing --- by pulling together software tools and simulation codes into a cohesive end-to-end framework. Customization is implemented by template and script files so that the same runtime code can be used for diverse applications. CMB implementations have been successfully developed this way for many technical domains including computational fluid dynamics, heat transfer, high-energy physics, hydrology, nuclear physics, and wave energy conversion.

All of the :program:`CMB` source code used in this :program:`CMB Wind Tunnel` example has been developed with an open source, permissive license. :program:`CMB` source code repositories are publicly available at https://gitlab.kitware.com/cmb/.

OpenFOAM
--------


.. image:: ./images/openfoam-bike.png
    :align: right
    :width: 50%


:program:`OpenFOAM` is a powerful open source CFD analysis tool suite with an extensive feature set. Text files are used to define each analysis, including, for example, problem physics, boundary conditions, time-step logic, options for generating discrete geometries (meshes),  etc. Creating and editing numerous text files can be challenging, especially as problem complexity increases. With :program:`CMB modelbuilder`, these files are generated automatically from a graphical (form style) interface so that you don't have to deal with typos, syntax, or looking up keywords. The :program:`modelbuilder` application also provides graphical (3D) display of modeling geometries for visual feedback, and organizes the workflow data, storing the :program:`OpenFOAM` files in a project directory, so that users do not have to remember or lookup file system paths.
