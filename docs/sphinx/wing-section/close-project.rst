Close the Project
===================

When you are done exploring the results, select the :menuselection:`Wind Tunnel --> Close Project` menu item to remove the project from memory and clear the various view panels. If you made any changes in the :guilabel:`OpenFOAM` tab, you will be prompted to save the project before closing it.
