Display the Velocity Field
=============================

.. |advanced| image:: ../images/pqAdvanced.svg
    :width: 24px

.. |auto_apply| image:: ../images/pqAutoApply.svg
    :width: 24px

.. |stream_tracer| image:: ../images/pqStreamTracer.svg
    :width: 24px


For the velocity data, we will use the :program:`ParaView` streamline filter.

.. rst-class:: step-number

1\. Disable Automatic Changes

Because the default streamline filter can consume significant CPU resources, first disable the default modelbuilder behavior to automatically apply changes to the render view. To do this, find the "Apply Changes Automatically" button ( |auto_apply| ) and click it to the off state (it's a toggle button).

.. image:: ../images/toolbar-autoapply.png
    :align: center

|


.. rst-class:: step-number

2\. Create Stream Tracer Filter

There are several ways to add a streamline filter to the display, the easiest is to find and click the "Stream Tracer" tool button ( |stream_tracer| ). You can also use the :menuselection:`Filters --> Common --> Stream Tracer` menu item.

.. image:: ../images/toolbar-streamtracer.png
    :align: center

|


.. rst-class:: step-number

3\. Click the Apply Button

Click the :guilabel:`Apply` button at the top of the :guilabel:`Properties` tab to update the 3D view. It might take several seconds for :program:`modelbuilder` to be responsive after this, because the default stream tracer settings can use alot of CPU cycles.


.. rst-class:: step-number

4\. Set the Line Geometry

To set the streamline properties, we will to open the :guilabel:`Properties` view (from which nearly all visualization features are accessed). To open this view, go to the :guilabel:`View` menu and check the box next to :guilabel:`Properties`. When the :guilabel:`Properties` view appears in the sidebar, undock it and drag it over the other tabbed views to dock it as another tab.

The :guilabel:`Properties` view has three top-level, collapsible sections labeled :guilabel:`Properties (StreamTracer1)`, :guilabel:`Display (GeometryRepresentation)`, and :guilabel:`View (Render View)`. Because the contents of the :guilabel:`Properties` view are extensive, we suggest that you collapse all three sections to start, and open them one at a time when working with this view.

To set the streamline properties, go to the :guilabel:`Properties` panel and open the first section, which is now labeled :guilabel:`Properties (StreamTracer1)`. In the :guilabel:`Line Parameters` subsection:

a. Set the :guilabel:`Point1` fields to ``0.0``   ``-0.6``  ``-0.1``.
b. Set the :guilabel:`Point2` fields to ``0.0``   ``0.6``  ``0.1``.
c. You can also uncheck the :guilabel:`Show Line` box.
d. Just below the :guilabel:`Line Parameters` subsection, set the :guilabel:`Resolution` field to ``40``.

.. image:: ../images/properties-streamtracer.png
    :width: 360
    :align: center

|


.. rst-class:: step-number

5\. Click the Apply Button

Click the :guilabel:`Apply` button at the top of the :guilabel:`Properties` tab to update the 3D view.


.. image:: ../images/wingsection-velocity.png
    :align: center

|


.. rst-class:: step-number

6\. More Visual Features

.. rst-class:: margin-top-1

**6.1 Wing section geometry**: If you want to include the wingsection model in the display, you can go back to the :guilabel:`OpenFoam` view (tab), select the :guilabel:`Model` sub-tab and click the :guilabel:`Show Data` button near the top of the tab.


.. rst-class:: margin-top-1

**6.2 Streamline tubes**: To display more aesthetic streamlines:

  * Close the :guilabel:`Properties (StreamTracer1)` section.
  * Click the |advanced| button (near the top of the :guilabel:`Properties` view) to display advanced properties.
  * Expand the :guilabel:`Display (GeometryRepresentation)` section.
  * Find the :guilabel:`Styling` subsection.
  * In the :guilabel:`Styling` subsection, set the :guilabel:`Line Width` field to ``4``.
  * In the :guilabel:`Styling` subsection, check the box next to :guilabel:`Render Lines As Tubes`.


.. rst-class:: step-number

7\. Enable Automatic Changes

Before leaving your work, be sure to re-enable the "Apply Changes Automatically" button ( |auto_apply| ). (Otherwise, you will need to click the :guilabel:`Apply` when you make other changes like importing geometry files.)
