Import the Target Geometry
============================

.. |nbsp| unicode:: 0xa0
    :trim:

We start with the :guilabel:`Model`, so make sure it is selected. Here is where we will import our target geometry into the project and then run OpenFOAM's :program:`surfaceFeatureExtract` program to detect model edges.

.. note::
  You can resize the :guilabel:`OpenFOAM` panel (dock widget) horizontally to see its full contents by dragging its right side.

.. image:: ../images/model-tab-empty.png
    :align: center

|

The :guilabel:`Model` tab is organized vertically in three sections labeled :guilabel:`Import Model`, :guilabel:`Surface Features`, and :guilabel:`surfaceFeatureExtract`. The first and third sections are *control views* that were added to this version of :program:`modelbuilder` for running operations. (Control views are indicated by a vertical bar near their left border.) The middle section is an *attribute view* for editing data that :program:`modelbuilder` will insert into OpenFOAM input files.

.. rst-class:: step-number

1\. Download Model File

We will be using the :file:`wing_5degrees.obj` file that is including in the :program:`OpenFOAM` code repository. You can download this file with the following link:

:download:`wing_5degrees.obj <../_downloads/wing_5degrees.obj>` |nbsp| |nbsp| (right click on the link and use the ":menuselection:`Save Link As...`" option)


.. rst-class:: step-number

2\. Import Into ModelBuilder

In the :guilabel:`Import Model` section, click the :guilabel:`Run` button to bring up a dialog for loading a model file. Click the :guilabel:`Browse` button to navigate to the :file:`wing_5degrees.obj` file you downloaded. With that file selected, the dialog should look like the following:

.. image:: ../images/wingsection-import.png
    :align: center

|

Click the dialog's :guilabel:`Apply` button. In response, :program:`modelbuilder` will read the file, create an internal representation for the geometry and topology, and display the result in the 3D view. The shape is not easily discerned in a static view, but if you manipulate the viewpoint with your mouse (left button to rotate) you will see that its shape is a basic airfoil.

.. image:: ../images/wingsection-model-loaded.png
    :align: center

|

.. rst-class:: step-number

3\. Surface Features

The :guilabel:`Surface Features` section has two purposes, to (i) specify the options used to determine edges to preserve when meshing the geometry, and (ii) specify a point inside the geometry for the meshing software that will be run later.

In this example, the default settings are suitable so you do not have to edit any of the fields.


.. rst-class:: step-number

4\. Run surfaceFeatureExtract

The last section in the :guilabel:`Model` tab is for running the OpenFOAM :program:`surfaceFeatureExtract` program, which detects features in the input geometry and writes them to the project directory. If you have the OpenFOAM :program:`Docker` image loaded as described in :doc:`../install`, click the :guilabel:`Run surfaceFeatureExtract` button. In response, :program:`modelbuilder` will set up and launch :program:`surfaceFeatureExtract` program and display its log file in a popup dialog. The run should only take a few seconds (unless you have not yet loaded the :program:`OpenFOAM` docker image, which will take minutes). The result should look like this.


.. image:: ../images/wingsection-surfaceFeatureExtract.png
    :align: center

|

At this point, the modeling geometry is set up and you can close the log file dialog.

Before moving on, also select the :menuselection:`WindTunnel --> Save Project` menu item to save the changes to your project so far.

Then proceed to the :guilabel:`Block Mesh` tab where we will set the wind tunnel geometry.
