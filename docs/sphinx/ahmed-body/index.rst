Example: Ahmed Body
=====================

The Ahmed Body is a geometric shape used during the early development of aerodynamic simulation software back in the 1980's [#ahmed]_. Despite its relatively simple shape, the Ahmed Body exhibits a number of predictable airflow features and quickly became a standard benchmark that is still used today. We are using it here for our second example to show a solution with full three-dimensional airflow. This example will mirror many of the same steps as the wing section, with the addition of mesh-refinement regions. We presume you have already completed the :ref:`Example Wing Section` section and have some familiarity with the menus and view panels.

.. image:: ../images/ahmedbody-pressure-surface.png
  :align: center

|

.. toctree::
    :maxdepth: 1

    create-project.rst
    import-geometry.rst
    blockmesh.rst
    refinemesh.rst
    snappyhexmesh.rst
    icofoam.rst
    display-pressure.rst
    display-velocity.rst
    close-project.rst

.. note::
    Screenshots in this section were taken from :program:`modelbuilder` running on a linux (Ubuntu) desktop machine.


.. [#ahmed] Ahmed, S., Ramm, G., and Faltin, G., "Some Salient Features Of The Time-Averaged Ground Vehicle Wake," SAE Technical Paper 840300, 1984, https://doi.org/10.4271/840300.
