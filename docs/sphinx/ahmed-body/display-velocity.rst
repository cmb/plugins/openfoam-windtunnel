Display the Velocity Field
=============================

.. |advanced| image:: ../images/pqAdvanced.svg
    :width: 24px

.. |auto_apply| image:: ../images/pqAutoApply.svg
    :width: 24px

.. |stream_tracer| image:: ../images/pqStreamTracer.svg
    :width: 24px


We will again use the :program:`ParaView` streamline filter to display the velocity field computed by :program:`OpenFOAM`.

.. rst-class:: step-number

1\. Set the Active Representation

Readers familiar with :program:`ParaView` know that the "active representation" is currently the slice filter we created in the previous page. Before creating a stream filter for velocity, we want to first (i) hide the slice plane, and (ii) set the :program:`icoFoam` output data as the active representation. To do these, go to the :guilabel:`Pipeline Browser` view.

* Click the eyebal next to ``Slice1`` to the off state (closed eye).
* Click the ``ico.foam`` item to select it.

.. rst-class:: with-border
.. image:: ../images/pipeline-icofoam.png
    :width: 360
    :align: center


.. rst-class:: step-number

2\. Create the Filter

Use the same steps as before to create the streamline filter.

* Turn off automatic updates by toggling the "Apply Changes Automatically" button ( |auto_apply| ) to the off state.
* Create the streamline filter by clicking the "Stream Tracer" tool button ( |stream_tracer| ) or using the :menuselection:`Filters --> Common --> Stream Tracer` menu item.
* Click the :guilabel:`Apply` button at the top of the :guilabel:`Properties` tab to update the 3D view. It might take several seconds for :program:`modelbuilder` to be responsive after this, because the default stream tracer settings can use alot of CPU cycles.


.. rst-class:: step-number

3\. Set the Line Geometry

In the :guilabel:`Properties` view, keep the first collapsible section (:guilabel:`Properties (StreamTracer1)`) open and collapse the other two. Set these properties in the :guilabel:`Line Parameters` subsection:

a. Set the :guilabel:`Point1` fields to ``0.0``   ``0.0``  ``0.1``.
b. Set the :guilabel:`Point2` fields to ``0.0``   ``0.5``  ``0.2``.
c. Just below the :guilabel:`Line Parameters` subsection, set the :guilabel:`Resolution` field to ``40``.
d. You can also uncheck the :guilabel:`Show Line` box.
e. Click the :guilabel:`Apply` button to update the 3D view.

.. rst-class:: step-number

4\. More Visual Features

.. rst-class:: margin-top-1

**4.1 Ahmed Body geometry**: Go back to the :guilabel:`OpenFoam` view (tab), select the :guilabel:`Model` sub-tab and click the :guilabel:`Show Data` button near the top of the tab.


.. rst-class:: margin-top-1

**4.2 Streamline tubes**: To display more aesthetic streamlines:

  * Close the :guilabel:`Properties (StreamTracer1)` section.
  * Click the |advanced| button (near the top of the :guilabel:`Properties` view) to display advanced properties.
  * Expand the :guilabel:`Display (GeometryRepresentation)` section.
  * Find the :guilabel:`Styling` subsection.
  * In the :guilabel:`Styling` subsection, set the :guilabel:`Line Width` field to ``4``.
  * In the :guilabel:`Styling` subsection, check the box next to :guilabel:`Render Lines As Tubes`.


.. image:: ../images/ahmedbody-velocity.png
  :align: center

|


.. rst-class:: step-number

5\. Enable Automatic Changes

Before leaving your work, be sure to re-enable the "Apply Changes Automatically" button ( |auto_apply| ). (Otherwise, you will need to click the :guilabel:`Apply` when you make other changes like importing geometry files.)
