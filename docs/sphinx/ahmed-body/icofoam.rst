Run the icoFoam Solver
========================

Once again, let's stick with the default physics to start, so click the :guilabel:`Run icoFoam` button to start the :program:`icoFoam` utility which solves the incompressible laminar Navier-Stokes equations. The solver will typically take 10-20 minutes to run while the log is displayed in a pop up dialog. The default simulation time settings compute 0.0 to 0.5 seconds with a time step of 0.005 sec and output every 20 steps (0.1 seconds). The simulation is done when the log shows the residuals and related data for "Time = 0.5" followed by the single line with the word "End". Close the dialog when the solver is done.
