Create a Project
==============================

.. |paraview_on| image:: ../images/postprocessing-mode_on@2x.png
    :width: 24px


To get started:

* Close the current project if there one is in memory. To do this, use the :menuselection:`WindTunnel --> Close Project` menu item.
* Hide the ParaView toolbars by clicking the "Enable ParaView" button ( |paraview_on| ).
* Select the :menuselection:`WindTunnel --> New Project...` menu item and provide a project directory to the :guilabel:`New Project` dailog that pops up. A suggested location is :file:`${HOME}/modelbuilder/projects/ahmedbody`. After creating and selecting the directory, return to the :guilabel:`New Project` dialog and click the :guilabel:`Apply` button.

When the dialog closes, :program:`modelbuilder` creates an empty project and the :guilabel:`OpenFOAM` dock widget (in the sidebar) and displays a set of tabs for user input. For a quick review:

* The :guilabel:`Model` tab is for importing the model geometry.
* The :guilabel:`Block Mesh` tab is for setting the wind tunnel geometry and generating the initial background mesh.
* The :guilabel:`Refine Mesh` tab is for optionally adding refinement regions to the background mesh.
* The :guilabel:`Snappy Hex Mesh` tab if for generating the analysis mesh.
* The :guilabel:`Solver` tab is for specifying the physics and running the solver.

.. image:: ../images/project-created.png
    :align: center

|
