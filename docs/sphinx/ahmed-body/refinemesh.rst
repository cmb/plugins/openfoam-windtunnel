Refine the Block Mesh
=======================

In order to successfully mesh the Ahmed body, we need to add one refinement region to the block mesh generated in the previous step. To do this, go to the :guilabel:`Refine Mesh` tab. The tab has three sections organized vertically, labeled :guilabel:`Refinement Directions`, :guilabel:`Refinement Regions`, :guilabel:`refineMesh`. We will be using the second and third sections.


.. rst-class:: step-number

1\. Create Refinement Region

The :guilabel:`Refinement Regions` section is slightly different than other parts of the user interface. In this section, you create one or more **attribute** instances, each representing one region in the mesh to apply a 2:1 edge refinement (splitting each hexadral cell by 8).

For the Ahmed Body, we need one refinement region.  In the :guilabel:`Refinement Regions` section, find and click the :guilabel:`New` button to create an attribute instance. A panel to edit its features appears below - you might have to scroll down to see it entirely. Enter these values for the :guilabel:`Box` coordinates:

::

    Position: -1.0   0.0  -0.6
    Scale:     5.0   1.0   1.2

.. image:: ../images/ahmedbody-refinewidget.png
    :align: center

|


.. rst-class:: step-number

2\. Run refineMesh

In the bottom section of the :guilabel:`Refine Mesh` tab, click the :guilabel:`Run refineMesh` button and wait for the dialog to finish, which should only take a few seconds. When complete, the last line in the dialog is "All refineMesh processing steps completed".


.. note::
  The :program:`refineMesh` utility overwrites the block mesh and is **NOT** idempotent. If you run :program:`refineMesh` twice you will refined the same region two times. If you make a mistake on this page, go back to the :guilabel:`Block Mesh` tab and rerun the :program:`blockMesh` utility before running :program:`refineMesh` again.


.. rst-class:: step-number

3\. Display the Mesh

Click the :guilabel:`Reload Data` to display the refined mesh. To see the refined region, rotate the view so that you can see the bottom side.

.. image:: ../images/ahmedbody-refinemesh.png
    :align: center

|

Before moving to the next tab, click the :guilabel:`Hide Data` button.
