Close the Project
===================

When you are done exploring the results, go to the :guilabel:`Wind Tunnel` menu and select the :guilabel:`Close Project` item to remove the project from memory and clear the various view panels. If you made any changes in the :guilabel:`OpenFOAM` tab, you will be prompted to save the project before closing it.
